<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>文件上传结果</title>
</head>
<body>
  <h2>${message1}</h2>
  <h2>${message2}</h2>
  <h2>${message3}</h2>
  <h2>${message4}</h2>
  <p>对象数据</p>
    ${message5}<br data-filtered="filtered">
    ${message5.name}<br data-filtered="filtered">
  <p>列表数据</p>
  <c:forEach items="${message6}" var="u">
    <tr>
      <td>${u.name}</td>
      <td>${u.age}</td>
      <td>${u.birth}</td>
    </tr>
    ${u.name}-${u.age}-${u.birth}<br data-filtered="filtered">
  </c:forEach>
</body>
</html>