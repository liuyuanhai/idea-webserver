<%@ page import="com.test.servlet.Circle" %>
<%@ page import="com.test.servlet.UploadServlet" %>
<%--
  Created by IntelliJ IDEA.
  User: liuyu
  Date: 2023/3/3
  Time: 11:18
  To change this template use File | Settings | File Templates.
--%>
<%-- 该部分注释在网页中不会被显示--%>
<%--中文编码问题--%>
<%--如果我们要在页面正常显示中文，我们需要在 JSP 文件头部添加以下代码：--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<head>
    <title>这是个测试网站</title>
</head>
<body>
<H1>测试标题</H1>
<div>666啊</div>
<div>
    <p>还打野</p>
    <p>还打野</p>
    <p>还打野</p>
    <p>今天的日期是: <%= (new java.util.Date()).toLocaleString()%></p>

</div>
<%--  脚本程序--%>
<%--  脚本程序可以包含任意量的Java语句、变量、方法或表达式，只要它们在脚本语言中是有效的。--%>
<%
    out.println("使用代码片段 1--------------开始");
    out.println("Your IP address is " + request.getRemoteAddr());
    out.println("使用代码片段 1--------------结束");
%>

<%--  JSP声明--%>
<%--  一个声明语句可以声明一个或多个变量、方法，供后面的Java代码使用。在JSP文件中，您必须先声明这些变量和方法然后才能使用它们。--%>
<%--  JSP声明的语法格式：--%>
<%--  <%! declaration; [ declaration; ]+ ... %>--%>
<%! int i = 0; %>
<%! int a, b, c; %>
<%! Circle circle = new Circle(2.0f,2.0f); %>
<jsp:scriptlet>
       out.println("使用代码片段 2--------------开始");
       out.println("Your IP address is " + request.getRemoteAddr());
        int e = 2;
        int f = 3;
           out.println ("e = "+e);
           out.println ("i = "+i);
       out.println("使用代码片段 2--------------结束");
</jsp:scriptlet>
<%--  同样，您也可以编写与之等价的XML语句--%>
<%--  <jsp:expression>--%>
<%--  </jsp:expression>--%>
<% if (i == 0) { %>
<p>今天是周末</p>
<% } else { %>
<p>今天不是周末</p>
<% } %>
<%
    switch(i) {
        case 0:
            out.println("星期天");
            break;
        case 1:
            out.println("星期一");
            break;
        case 2:
            out.println("星期二");
            break;
        case 3:
            out.println("星期三");
            break;
        case 4:
            out.println("星期四");
            break;
        case 5:
            out.println("星期五");
            break;
        default:
            out.println("星期六");
    }
%>
<%-- 生命周期 初始化和销毁--%>
<%!
    private int initVar=0;
    private int serviceVar=0;
    private int destroyVar=0;
    public void jspInit(){
        initVar++;
    }
//    //执行
//    public void _jspService(HttpServletRequest request,
//                     HttpServletResponse response)
//    {
//        destroyVar++;
//        // 服务端处理代码
//    }
    public void jspDestroy(){
        destroyVar++;
    }
%>
<%
    String content1="初始化次数 : "+initVar;
    String content2="响应客户请求次数 : "+serviceVar;
    String content3="销毁次数 : "+destroyVar;
    out.println(content1);
    out.println(content2);
    out.println(content3);
%>
<h1>文件上传实例 - 菜鸟教程</h1>
<form method="post" action="/UploadServlet" enctype="multipart/form-data">
    选择一个文件:
    <input type="file" name="uploadFile" />
    <br/><br/>
    <input type="submit" value="上传" />
</form>

<a href="testjump.html">跳转</a><br><br>
<a href="testjump2.html">跳转到第二个页面</a><br><br>
<!-- 页面跳转的几种方式-->
<a href="one.action">1.普通转发页面(对请求的默认处理方式)</a><br><br>

<a href="two.action">2.跳转第二中方式 action重定向 forward</a><br><br>

<a href="three.action">3.跳转第二中方式 action重定向 redirect</a><br><br>

<a href="four.action">4.普通html重定向页面 forward</a><br><br>
<a href="five.action">5.普通html重定向页面 redirect</a><br><br>
<a href="message/message">带数据跳转</a><br><br>
<a href="message/message2">带数据跳转2</a><br><br>

</body>
</html>
