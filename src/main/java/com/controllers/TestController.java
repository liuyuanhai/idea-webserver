package com.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class TestController {
    @RequestMapping
    public String index(){
        return "index";
    }
    @RequestMapping("/message")
    public String message(){
        return "message";
    }

    /**
     * 带数据跳转倒网页
     * @param model
     * @return
     */
    @RequestMapping("/message/message")
    public String message(Model model){
        //传递string
        model.addAttribute("message1","测试带数据跳转1");
        model.addAttribute("message2","测试带数据跳转2");
        model.addAttribute("message3","测试带数据跳转3");
        model.addAttribute("message4","测试带数据跳转4");
        //传递对象
        User user = new User("张三", 12, new Date());
        //传递对象列表
        List<User> us= new ArrayList<User>();
        us.add(new User("张三", 12, new Date()));
        us.add(new User("张四", 13, new Date()));
        us.add(new User("张五", 14, new Date()));
        model.addAttribute("message5",user);
        model.addAttribute("message6",us);
        return "message";
    }
    /**
     * 得到请求参数
     * @param model
     * @return
     */
    @RequestMapping("/message/message2")
    public String message2(HttpServletRequest request, Model model){
        String username = request.getParameter("username");
        model.addAttribute("msg",username);
        return "message2";
    }

    /**
     * 需要传参数
     * @param username
     * @param model
     * @return
     */
    @RequestMapping("/message/message3")
    public String message3(@RequestParam String username, Model model){

        return "message3";
    }

    @RequestMapping("/testjump")
    public String jumpTest(){
        return "testjump";
    }
    @RequestMapping("/testjump2")
    public String jumpTest2(){
        return "testjump2";
    }

    //跳转第一中方式,普通跳转
    @RequestMapping("/one")
    public String one(){
        System.out.println("one action被访问......");
        return "testjump";
    }
    /**
     *  跳转第二中方式 action重定向
     * 下面是我们注册的视图解析器的父类：UrlBasedViewResolver，中的几个参数
     *  通过对底层源代码的解读，可知在action方法的返回值字符串中，如果以"redirect:"或者"forward:"开头则不会执行视图解析器的路径拼接
     *     而是会按照redirect或forward完成页面重定向或页面跳转
     *
     *     public static final String REDIRECT_URL_PREFIX = "redirect:";
     *     public static final String FORWARD_URL_PREFIX = "forward:";
     *     private String prefix = "";
     *     private String suffix = "";
     *     注意：不管要使用action的页面转发或者是action的页面重定向，由于action方法是控制器内部的方法
     *     所以要想访问action方法必须访问到控制SpringMVC控制器，而要访问控制器，前提是要能被SpringMVC核心处理器处理(也就是底层的servlet)
     *     而要想被底层servlet处理，必须满足请求路径的通配条件，这是我们在web.xml文件中配置好的"*.action"
     *     所以要在请求的末尾加上".action"以满足请求的通配要求，才有资格被交给SpringMVC的控制器中的方法处理
     */

    @RequestMapping("/two")
    public String two(){
        System.out.println("other action被访问......");
        return "forward:/one.action";
    }
    @RequestMapping("/three")
    public String three(){
        System.out.println("other action被访问....");
        return "redirect:/one.action";
    }
    //跳转第三中方式 如果是普通重定向，直接重定向到项目资源，不需要控制器中的action方法的处理
    @RequestMapping("/four")
    public String four(){
        System.out.println("three action被访问......");
        return "forward:/testjump";  //"forward:/testjump"或"forward:/testjump.html"均可,.html后缀会被忽略。如果是"forward:/testjump.jsp“，则会报错。
    }
    //跳转第三中方式
    @RequestMapping("/five")
    public String five(){
        System.out.println("three action被访问......");
        return "redirect:/testjump";//"forward:/testjump"或"forward:/testjump.html"均可,.html后缀会被忽略。如果是"forward:/testjump.jsp“，则会报错。
    }

}
