package com.test.servlet;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * java 1.8.1 测试正常 ；17.2正常
 * 使用 auth0.java.jwt包 (Maven搜索使用 com.auth0:java-jwt)
 * 注意 不能同时使用 auth0.java.jwt包 和 io.jsonwebtoken.jjwt包 否则会因为包冲突导致无法生成签名token
 *      如果需要同时使用：请删除io.jsonwebtoken.jjwt下的jackson相关的所有包
 */
public class JwtTokenUtil {
    //设置过期时间
    private static final long EXPIRE_DATE=30l*60*100000;
    //token秘钥
    private static final String TOKEN_SECRET = "123456789";
    public static void test(){
        String username ="zhangsan";
        String password = "123";
        String token = JwtTokenUtil.createToken(username,password);
        System.out.println(token);
        boolean b = JwtTokenUtil.verify(token);
        System.out.println(b);
        JwtTokenUtil.parseToken(token);
    }
    public static String createToken(String username,String password){

        String token = "";
        try {
            //过期时间
            Date date = new Date(System.currentTimeMillis()+EXPIRE_DATE);
            //秘钥及加密算法
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            //设置头部信息
            Map<String,Object> header = new HashMap<>();
            header.put("typ","JWT");
            header.put("alg","HS256");
            //携带username，password信息，生成签名
            token = JWT.create()
                    .withHeader(header)
                    .withClaim("username",username)
                    .withClaim("password",password)
                    .withExpiresAt(date)
                    .sign(algorithm);
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
        return token;
    }
    public static boolean verify(String token){
        /**
         * @desc   验证token，通过返回true
         * @create 2019/1/18/018 9:39
         * @params [token]需要校验的串
         **/
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        }catch (Exception e){
            return  false;
        }
    }

    /***
     * 获取值信息 Map
     * @param token
     * @return
     * @throws Exception
     */
    public static Map<String, Claim> getClaims(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            // e.printStackTrace();
            // token 校验失败, 抛出Token验证非法异常
        }
        return jwt.getClaims();
    }

    /**
     * 根据Token 解析数据
     * @param token
     * @return user_id
     */
    public static void parseToken(String token) {
        Map<String, Claim> claims = getClaims(token);
        Claim usernameClaim = claims.get("username");
        Claim passwordClaim = claims.get("password");

        if (null == usernameClaim || StringUtils.isEmpty(usernameClaim.asString())) {
            // token 校验失败, 抛出Token验证非法异常
        }else{
            System.out.println("username = "+usernameClaim.asString());
        }
        if (null == passwordClaim || StringUtils.isEmpty(passwordClaim.asString())) {
            // token 校验失败, 抛出Token验证非法异常
        }else{
            System.out.println("password = "+passwordClaim.asString());
        }
    }
}
