package com.test.servlet;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * java sdk 1.8.1 测试正常 ,17.2测试不正常（新版本的java sdk中缺少类：javax/xml/bind/DatatypeConverter）
 * 使用 io.jsonwebtoken.jjwt包 (Maven搜索使用io.jsonwebtoken:jjwt)
 */
public class JsonWebTokenUtil {
    public static long VALIDITY_TIME_MS = 30l*(60*60*24*1000);//有效期 30天
    public static String secret ="1234567890";//密钥
    public static  void test(){
        String token = JsonWebTokenUtil.createToken("liuyuanhai",000001l,"aa.jpg","222@qq.com");
        System.out.println(token);
        JsonWebTokenUtil.isTokenValid(token);
        JsonWebTokenUtil.parseToken(token);
    }

    /**
     * 从用户中创建一个jwt Token
     * @return token
     */
    public static String createToken(String username,long id,String avatar,String email) {
        return Jwts.builder()
                .setExpiration(new Date(System.currentTimeMillis() + VALIDITY_TIME_MS))
                .setSubject(username)
                .claim("id", id)
                .claim("avatar", avatar)
                .claim("email",email)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    /**
     * 检测token 是否过期
     * @param token
     */
    public static boolean isTokenValid(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            System.out.println("token 正常");
            return true;
        }catch (ExpiredJwtException e){
            System.out.println("token 过期");
            return false;
        }

    }
    /**
     * 从token中取出用户
     */
    public static void parseToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            System.out.println(claims.get("id",Long.class));
            System.out.println(claims.get("avatar",String.class));
            System.out.println(claims.get("email",String.class));
            System.out.println(claims.getExpiration());
        }catch (ExpiredJwtException e){
            System.out.println("token 过期");
        }catch(Exception e){
            System.out.println("token 异常");
        }
    }

}
