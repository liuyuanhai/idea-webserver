package com.test.servlet;

import java.sql.SQLException;

import static com.test.servlet.ConnectMySqlUtil.connection;

public class MySqlUtil {
    //向数据库添加一条数据
    public static void addOneDataToTestTable1(){//向表中插入一条数据
        String sql="insert into test_table1 (student_id, param2, param3, param4) values(?, ?, ?, ?)";

        java.sql.PreparedStatement ptmt = null;
        try {
            ptmt = connection.prepareStatement(sql);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            ptmt.setInt(1, 1001);
            ptmt.setString(2, "男");
            ptmt.setString(3, "学生002");
            ptmt.setString(4, "无所为");
            ptmt.execute();//执行给定的SQL语句，该语句可能返回多个结果
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addArrayDataToTestTable1(){//向表中插入多条数据
        String sql="insert into test_table1 (student_id, param2, param3, param4) values(?, ?, ?, ?)";

        java.sql.PreparedStatement ptmt = null;
        try {
            connection.setAutoCommit(false);// 关闭事务
            ptmt = connection.prepareStatement(sql);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        for (int i=0;i<10;i++) {

            try {
                ptmt.setInt(1, 2000+i);
                ptmt.setString(2, "男");
                ptmt.setString(3, "学生"+i);
                ptmt.setString(4, "无所为");
                ptmt.execute();//执行给定的SQL语句，该语句可能返回多个结果
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            ptmt.executeBatch();//执行给定的SQL语句，该语句可能返回多个结果
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void updateOneData(){//更新单挑数据
        String sql="update test_table1 set student_id=? where student_id = ?";
        java.sql.PreparedStatement ptmt = null;
        try {
            ptmt = connection.prepareStatement(sql);

        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            ptmt.setInt(1,10000);
            ptmt.setInt(2,2);
            ptmt.execute();//执行给定的SQL语句，该语句可能返回多个结果
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void updateArrayData(){//更新多条数据
        String sql="update test_table1 set student_id=? where student_id = ?";
        java.sql.PreparedStatement ptmt = null;
        try {
            connection.setAutoCommit(false);// 关闭事务
            ptmt = connection.prepareStatement(sql);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            for(int i=0;i<10;i++){
                ptmt.setInt(1,4000+i);
                ptmt.setInt(2,2000+i);
                ptmt.execute();//执行给定的SQL语句，该语句可能返回多个结果
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ptmt.executeBatch();//执行给定的SQL语句，该语句可能返回多个结果
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
