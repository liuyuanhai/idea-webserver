package com.test.servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectMySqlUtil {


    /**
     *
     * @param url
     * @param user
     * @param password
     * 例如：   String url = "jdbc:mysql://local:3306/"+"test_schema";//数据库地址+数据库名称
     *         String user = "root";//数据库用户
     *         String password = "123456";//数据库密码
     */
    public static Connection connection;
    public static Connection connectMySQL(String url,String user,String password){
        try {
            connection = DriverManager.getConnection(url,user,password);
            return connection;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void closeMysqlConnection(Connection connection){
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
