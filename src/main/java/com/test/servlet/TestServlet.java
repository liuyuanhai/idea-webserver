package com.test.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "TestServlet", value = "/TestServlet")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        //解析参数
        String name = request.getParameter("name");
        String passwod = request.getParameter("password");
        System.out.println("name = "+name);
        System.out.println("passwod = "+passwod);
        //返回数据
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        if(name.equals("1000")&&passwod.equals("123456")){
            response.getWriter().write("登录成功");
        }else{
            response.getWriter().write("登录失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
