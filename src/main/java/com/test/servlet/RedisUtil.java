package com.test.servlet;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RedisUtil {
    private static Jedis jedis;
    public static boolean connectToRedis(){
        try {
            Jedis jedis = new Jedis("localhost");
            jedis.auth("123456");//使用密码连接
            System.out.println("redis连接成功！！");
            return true;
        }catch (Exception e){
            System.out.println("redis连接失败！！");
        }
       return false;
    }
    //保存字符串
    public static void saveStringData(String key,String value){
        if(jedis!=null&&jedis.isConnected()){
            jedis.set(key,value);
        }
    }
    //取出字符串数据
    public static String getStringValue(String key){
        if(jedis!=null&&jedis.isConnected()){
            return jedis.get(key);
        }
        return null;
    }
    //保存字符串列表
    public static void saveListData(String key,String[] strs){
        if(jedis!=null&&jedis.isConnected()){
            jedis.lpush(key,strs);
        }
    }
    /**
     * 取出字符串列表数据
     */
    public static List<String> getLisData(String key,int start,int end){
        if(jedis!=null&&jedis.isConnected()) {
            return jedis.lrange(key,start,end);
        }
        return null;
    }
    //保存排名数据 添加一条数据
    public static void saveStortedData(String key,int score,String value){
        if(jedis!=null&&jedis.isConnected()) {
            jedis.zadd(key, score,value);
        }
    }

    /**
     * 取出排名数据
     * @param key
     * @param start
     * @param end
     */
    public static Set<String> getSortedData(String key, int start, int end){
        if(jedis!=null&&jedis.isConnected()) {
           return jedis.zrange(key,start,end);
        }
        return null;
    }

    /**
     * 设置过期时间
     * @param key
     * @param time 过期时长
     * @return
     */
    public static boolean setExpire(String key,int time){
        if(jedis!=null&&jedis.isConnected()) {
            jedis.expire(key,time);
        }
        return false;
    }
    /**
     * 设置过期时间
     * @param key
     * @param time 过期时长 unix时间戳（10位时间）
     * @return
     */
    public static boolean setExpireUnix(String key,int time){
        if(jedis!=null&&jedis.isConnected()) {
            jedis.expireAt(key,time);
        }
        return false;
    }
    /**
     * 设置过期时间
     * @param key
     * @param time 过期时长 Windows时间戳(13位时间)
     * @return
     */
    public static boolean setExpireWindows(String key,int time){
        if(jedis!=null&&jedis.isConnected()) {
            jedis.pexpireAt(key,time);

        }
        return false;
    }

    /**
     * 得到过期时间
     * @param key
     * @return unix时间戳（10位时间）
     */
    public static long getExpireUnix(String key){
        return jedis.ttl(key);
    }

    /**
     * 得到过期时间
     * @param key
     * @return Windows时间戳(13位时间)
     */
    public static long getExpireWidnows(String key){
        return jedis.pttl(key);
    }

}
